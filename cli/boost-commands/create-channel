#!/usr/bin/env bash

if [[ $1 == "-h" ]] || [[ $1 == "--help" ]];then
  echo "this command creates channels from verified phone numbers. valid options are:"
  echo ""
  echo "-p : phone number of channel to activate";
  echo "-n : name to give to the channel once it is activated";
  echo "-a : comma-delimited list of admin phone numbers";
  echo "-u : url to target (in dev, use signalboost.ngrok.io)";
  echo ""
  exit 1
fi

echo "--- checking environment..."

pushd `pwd` > /dev/null # store current dir
cd `dirname "$0"` # cd to script path
source ../../.env # source env vars

if [ -z $SIGNALBOOST_API_TOKEN ];then
  echo "--- ERROR: no SIGNALBOOST_API_TOKEN found. Try \`make _.unlock\` then run again."
  exit 1
fi

if [ -z $(which jq) ];then
  echo "--- ERROR: you need the jq program installed to run this command!"
  exit 1
fi

while getopts ":p:n:a:u:" opt; do
  case "$opt" in
    p)
      phone_number="$OPTARG"
      ;;
    n)
      name="$OPTARG"
      ;;
    a)
      admins="$OPTARG"
      ;;
    u)
      url="$OPTARG"
      ;;
  esac
done

if [[ ! $phone_number =~ ^\+[0-9]{9,15}$ ]];then
  echo "> ERROR: -p must be a valid phone number prefixed by a country code"
  exit 1
fi

if [ -z "$name" ];then
  echo "> ERROR: -n (channel name) may not be empty"
  exit 1;
fi

if [ -z "$admins" ]
then
  echo "> ERROR: you must provide at least one sender phone number after the -s flag to create a channel"
  exit 1
elif [[ ! $admins =~ ^(\+[0-9]{9,15}(,?))+$ ]];then
  echo "> ERROR: -a must be a  comma-delimited list of valid phone numbers prefixed by a country code"
  exit 1
else
  # convert comma-delimited list of numbers
  # into string representation of array of string representations of numbers
  if [ "$(uname)" == 'Darwin' ]
    then # we are in mac and must use -E
       admins="[\"$(echo $admins | sed -E "s/,/\", \"/g")\"]"
    else # we are in linux and may use -re
       admins="[\"$(echo $admins | sed -re "s/,/\", \"/g")\"]"
  fi
fi

if [ -z $url ];then url=${SIGNALBOOST_HOST_URL}; fi

echo "--- provisioning new channel [${name}] on phone number ${phone_number} at url ${url}"

curl -s -X POST \
     -H "Content-Type: application/json" \
     -H "Token: $SIGNALBOOST_API_TOKEN" \
     -d "{ \"phoneNumber\": \"$phone_number\", \"name\": \"$name\", \"admins\":$admins }" \
     https://${url}/channels | jq

popd > /dev/null # return to starting directory
